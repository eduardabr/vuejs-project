const inMemoryData = localStorage.cars;

const state = {
  data: inMemoryData ? JSON.parse(inMemoryData) : [],
};

const actions = {
  addCar({ commit }, data) {
    commit("ADD_CAR", data);
  },
  editCar({ commit }, data) {
    commit("EDIT_CAR", data);
  },
  deleteCar({ commit }, id) {
    commit("DELETE_CAR", id);
  },
};

const mutations = {
  ADD_CAR(state, data) {
    data.id = setNewId(state.data);
    state.data.push(data);
    localStorage.setItem("cars", JSON.stringify(state.data));
  },

  EDIT_CAR(state, data) {
    const carIndex = state.data.findIndex((item) => item.id === data.id);
    state.data[carIndex] = data;
    localStorage.setItem("cars", JSON.stringify(state.data));
  },

  DELETE_CAR(state, id) {
    const carIndex = state.data.findIndex((item) => item.id === id);
    state.data.splice(carIndex, 1);
    localStorage.setItem("cars", JSON.stringify(state.data));
  },
};

function setNewId(existentCars) {
  let id = 0;
  if (existentCars.length > 0) {
    let lastId = existentCars[existentCars.length - 1].id;
    id = lastId + 1;
  }
  return id;
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};
