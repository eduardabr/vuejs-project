import { createStore } from "vuex";

// import modules
import cars from "./modules/cars";

export default createStore({
  modules: {
    cars,
  },
});
