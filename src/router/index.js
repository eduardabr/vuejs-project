import { createRouter, createWebHistory } from "vue-router";

// components
import Home from "../views/Home.vue";
import Management from "../views/Management.vue";
import Viewer from "../views/Viewer.vue";
import ManageCars from "../views/ManageCars.vue";
import ViewerAudi from "../components/ViewerAudi";
import ViewerVW from "../components/ViewerVW";

// defined routes with name & layout
const routes = [
  {
    path: "/",
    name: "home",
    component: Home,
  },
  {
    path: "/management",
    name: "management",
    component: Management,
  },
  {
    path: "/cars/:id?",
    name: "manage.cars",
    component: ManageCars,
    props: true,
  },
  {
    path: "/viewer",
    name: "viewer",
    component: Viewer,
    children: [
      {
        path: "vw",
        name: "viewer.vw",
        component: ViewerVW,
        meta: { specificLayout: true },
      },

      {
        path: "audi",
        name: "viewer.audi",
        component: ViewerAudi,
        meta: { specificLayout: true },
      },
    ],
  },
];

// creating router instance
const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
