import "./assets/scss/main.scss";

import { createApp } from "vue";

// services
import App from "./App.vue";
import router from "./router";
import store from "./store";

// create app
const app = createApp(App);

// uses
app.use(store).use(router);

// mount app to div
app.mount("#app");
